/*
 * ref-physician-doi-plugin: org.nrg.xnatx.refphysiciandoi.identifiers.ReferringPhysicianNameDicomObjectIdentifier
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.refphysiciandoi.identifiers;

import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.dcm4che2.data.Tag;
import org.nrg.dcm.ContainedAssignmentExtractor;
import org.nrg.dcm.Extractor;
import org.nrg.dcm.TextExtractor;
import org.nrg.dcm.id.ClassicDicomObjectIdentifier;
import org.nrg.dcm.id.CompositeDicomObjectIdentifier;
import org.nrg.dcm.id.TextDicomIdentifier;
import org.nrg.xnat.services.cache.UserProjectCache;
import org.nrg.xnat.utils.XnatUserProvider;

import java.util.regex.Pattern;

/**
 * Works exactly the same as the {@link ClassicDicomObjectIdentifier} implementation, except that this also supports using
 * the <b>ReferringPhysicianName</b> (0008,0090) DICOM header to map the project, subject, and session label for the
 * incoming DICOM.
 */
@Slf4j
public class ReferringPhysicianNameDicomObjectIdentifier extends CompositeDicomObjectIdentifier {
    public ReferringPhysicianNameDicomObjectIdentifier(final String name, final XnatUserProvider userProvider, final UserProjectCache userProjectCache) {
        super(name,
              new ExtensibleDicomProjectIdentifier(userProjectCache, new TextDicomIdentifier(Tag.ReferringPhysicianName)),
              new ImmutableList.Builder<Extractor>().add(new TextExtractor(Tag.PatientID))
                                                    .add(new ContainedAssignmentExtractor(Tag.ReferringPhysicianName, "Subject", Pattern.CASE_INSENSITIVE))
                                                    .add(new ContainedAssignmentExtractor(Tag.PatientComments, "Subject", Pattern.CASE_INSENSITIVE))
                                                    .add(new ContainedAssignmentExtractor(Tag.StudyComments, "Subject", Pattern.CASE_INSENSITIVE))
                                                    .add(new TextExtractor(Tag.PatientName))
                                                    .build(),
              new ImmutableList.Builder<Extractor>().add(new TextExtractor(Tag.StudyID))
                                                    .add(new ContainedAssignmentExtractor(Tag.ReferringPhysicianName, "Session", Pattern.CASE_INSENSITIVE))
                                                    .add(new ContainedAssignmentExtractor(Tag.PatientComments, "Session", Pattern.CASE_INSENSITIVE))
                                                    .add(new ContainedAssignmentExtractor(Tag.StudyComments, "Session", Pattern.CASE_INSENSITIVE))
                                                    .build(),
              new ImmutableList.Builder<Extractor>().add(new ContainedAssignmentExtractor(Tag.ReferringPhysicianName, "AA", Pattern.CASE_INSENSITIVE))
                                                    .add(new ContainedAssignmentExtractor(Tag.PatientComments, "AA", Pattern.CASE_INSENSITIVE))
                                                    .add(new ContainedAssignmentExtractor(Tag.StudyComments, "AA", Pattern.CASE_INSENSITIVE))
                                                    .build());
        setUserProvider(userProvider);
        log.info("Created the ReferringPhysicianNameDicomObjectIdentifier instance with name {}", name);
    }
}
