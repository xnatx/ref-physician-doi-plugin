/*
 * ref-physician-doi-plugin: org.nrg.xnatx.refphysiciandoi.identifiers.ExtensibleDicomProjectIdentifier
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.refphysiciandoi.identifiers;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.Tag;
import org.nrg.config.entities.Configuration;
import org.nrg.dcm.id.*;
import org.nrg.framework.configuration.ConfigPaths;
import org.nrg.xdat.XDAT;
import org.nrg.xft.XFT;
import org.nrg.xnat.services.cache.UserProjectCache;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
class ExtensibleDicomProjectIdentifier extends DbBackedProjectIdentifier {
    @SuppressWarnings("WeakerAccess")
    protected ExtensibleDicomProjectIdentifier(final UserProjectCache userProjectCache, final DicomDerivedString... identifiers) {
        super(userProjectCache);
        if (identifiers != null && identifiers.length > 0) {
            log.info("Adding {} extra identifiers", identifiers.length);
            _identifiers.addAll(Arrays.asList(identifiers));
        }
        log.info("Adding default identifiers");
        _identifiers.add(new ContainedAssignmentDicomIdentifier(Tag.PatientComments, "Project", Pattern.CASE_INSENSITIVE));
        _identifiers.add(new ContainedAssignmentDicomIdentifier(Tag.StudyComments, "Project", Pattern.CASE_INSENSITIVE));
        _identifiers.add(new TextDicomIdentifier(Tag.StudyDescription));
        _identifiers.add(new TextDicomIdentifier(Tag.AccessionNumber));
    }

    protected List<DicomDerivedString> getIdentifiers() {
        if (!_configLoaded) {
            log.info("The configuration was not previously loaded, now loading");
            loadFrom15Config(_identifiers);
        }
        return _identifiers;
    }

    private void loadFrom15Config(final Collection<DicomDerivedString> identifiers) {
        _configLoaded = true;
        try {
            final Reader        source;
            final Configuration configuration = XDAT.getConfigService().getConfig("dicom", "projectRules");
            final ConfigPaths   paths         = XDAT.getContextService().getBeanSafely(ConfigPaths.class);
            final Path          confDir       = Paths.get(XFT.GetConfDir());
            if (!paths.contains(confDir)) {
                paths.add(confDir);
            }
            final List<File> configs = paths.findFiles(DICOM_PROJECT_RULES);
            if (configuration != null && configuration.isEnabled() && StringUtils.isNotBlank(configuration.getContents())) {
                source = new StringReader(configuration.getContents());
            } else if (configs.size() > 0) {
                source = new FileReader(configs.get(0));
            } else {
                source = null;
            }

            if (source != null) {
                try (final BufferedReader reader = new BufferedReader(source)) {
                    String line;
                    while (null != (line = reader.readLine())) {
                        // Check if line's been added. Protects against duplicate rules, since PatternDicomIdentifer doesn't
                        // have its own equals() method to compare based on contents.
                        if (!_rulesFromConfigs.contains(line)) {
                            _rulesFromConfigs.add(line);
                            final DicomDerivedString extractor = parseRule(line);
                            if (null != extractor) {
                                identifiers.add(extractor);
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException ignored) {
            //
        } catch (IOException e) {
            log.error("An error occurred trying to open the DICOM project rules configuration", e);
        }
    }

    private static DicomDerivedString parseRule(final String rule) {
        final Matcher matcher = CUSTOM_RULE_PATTERN.matcher(rule);
        if (matcher.matches()) {
            final int    tag      = Integer.decode("0x" + matcher.group(1) + matcher.group(2));
            final String regexp   = matcher.group(3);
            final String groupIdx = matcher.group(4);
            final int    group    = null == groupIdx ? 1 : Integer.parseInt(groupIdx);
            return new PatternDicomIdentifier(tag, Pattern.compile(regexp), group);
        } else {
            return null;
        }
    }

    @SuppressWarnings("RegExpRedundantEscape")
    private static final Pattern CUSTOM_RULE_PATTERN = Pattern.compile("\\((\\p{XDigit}{4})\\,(\\p{XDigit}{4})\\):(.+?)(?::(\\d+))?");
    private static final String  DICOM_PROJECT_RULES = "dicom-project.rules";

    private final List<DicomDerivedString> _identifiers      = new ArrayList<>();
    private final List<String>             _rulesFromConfigs = new ArrayList<>();

    private boolean _configLoaded = false;
}
