package org.nrg.xnatx.refphysiciandoi;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.DicomObjectIdentifier;
import org.nrg.xnat.services.cache.UserProjectCache;
import org.nrg.xnat.utils.XnatUserProvider;
import org.nrg.xnatx.refphysiciandoi.identifiers.ReferringPhysicianNameDicomObjectIdentifier;
import org.springframework.context.annotation.Bean;

@XnatPlugin(value = "ref-physician-doi-plugin", name = "CNDA Referring Physician DICOM Object Identifier Plugin")
@Slf4j
public class ReferringPhysicianDOIPlugin {
    @Bean
    public DicomObjectIdentifier<XnatProjectdata> referringPhysicianDicomObjectIdentifier(final XnatUserProvider receivedFileUserProvider, final UserProjectCache userProjectCache) {
        log.info("Creating the ReferringPhysicianNameDicomObjectIdentifier");
        return new ReferringPhysicianNameDicomObjectIdentifier("Referring Physician DICOM Object Identifier", receivedFileUserProvider, userProjectCache);
    }
}
