# CNDA Referring Physician DICOM Object Identifier Plugin #

This is the CNDA Referring Physician DICOM Object Identifier (DOI) Plugin. It supports mapping the destination
project, subject, and session label values in the ReferringPhysicianName (0008,0090) DICOM header tag in the form:

```
0008,0090 Project:PROJECT_ID Subject:SUBJECT_ID Session:SESSION_ID
```

# Building #

To build the plugin:

1. If you haven't already, clone [this repository](https://bitbucket.org/xnatx/ref-physician-doi-plugin.git) and cd to the newly cloned folder.

1. Build the plugin:

    `./gradlew clean jar distZip` 
    
    On Windows, you can use the batch file:
    
    `gradlew.bat clean jar distZip`
    
    This should build the plugin in the file **build/libs/ref-physician-doi-plugin-1.0.0-SNAPSHOT.jar** 
    (the version may differ based on updates to the code).
    
1. Copy the plugin jar to your plugins folder: 

    `cp build/libs/ref-physician-doi-plugin-1.0.0-SNAPSHOT.jar /data/xnat/home/plugins`

# Deploying #

## Plugin ##

Deploying your XNAT plugin requires the following steps:

1. Copy the plugin jar to the **plugins** folder for your XNAT installation. The location of the 
**plugins** folder varies based on how and where you have installed your XNAT. If you are running 
a virtual machine created through the [XNAT Vagrant project](https://bitbucket/xnatdev/xnat-vagrant.git),
you can copy the plugin to the appropriate configuration folder and then copy it within the VM from 
**/vagrant** to **/data/xnat/home/plugins**.

1. Restart the Tomcat server.

Your new plugin will be available as soon as the restart and initialization process is completed.

